import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfasturactureComponent } from './infasturacture.component';

describe('InfasturactureComponent', () => {
  let component: InfasturactureComponent;
  let fixture: ComponentFixture<InfasturactureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfasturactureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfasturactureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
