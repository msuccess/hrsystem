import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TelemaskComponent } from './telemask.component';

describe('TelemaskComponent', () => {
  let component: TelemaskComponent;
  let fixture: ComponentFixture<TelemaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TelemaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TelemaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
