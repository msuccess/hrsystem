import { AbstractControl, ValidatorFn, Validator } from '@angular/forms';
import * as moment from 'moment';

export class CustomValidator  {
  static ageValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const age = moment().diff(moment(control.value));
      return age < 18 ? { 'invalidAge': true} : null;
    };
  }
}


