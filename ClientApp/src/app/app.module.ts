
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';



import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { ValidateanimateDirective } from './shared/directives/validateanimate.directive';
import { UndertakingsComponent } from './components/undertakings/undertakings.component';
import { ManufacturingComponent } from './components/manufacturing/manufacturing.component';
import { RegisterComponent } from './auth/register/register.component';
import { AgroComponent } from './components/agro/agro.component';
import { MiningComponent } from './components/mining/mining.component';
import { ReportComponent } from './components/report/report.component';
import { TelemaskComponent } from './components/telemask/telemask.component';
import { UserComponent } from './components/user/user.component';
import { AccountComponent } from './components/account/account.component';
import { AdminComponent } from './components/admin/admin.component';
import { AquaComponent } from './components/aqua/aqua.component';
import { EnergyComponent } from './components/energy/energy.component';
import { HomeComponent } from './components/home/home.component';
import { HrComponent } from './components/hr/hr.component';
import { InfasturactureComponent } from './components/infasturacture/infasturacture.component';




const routes: Routes = [
  { path: 'undertaking', component: UndertakingsComponent },
  { path: 'manufacturing', component: ManufacturingComponent },
  { path: 'home', component: HomeComponent },
  { path: 'tele', component: TelemaskComponent  },
  { path: 'energy', component: EnergyComponent },
  { path: 'agro', component: AgroComponent },
  { path: 'mining', component: MiningComponent  },
  { path: 'infrastracture', component: InfasturactureComponent },
  { path: 'aqua', component: AquaComponent },
  { path: 'register', component: RegisterComponent},
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: '**', component: HomeComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ValidateanimateDirective,
    RegisterComponent,
    UndertakingsComponent,
    ManufacturingComponent,
    AccountComponent,
    AdminComponent,
    AgroComponent,
    AquaComponent,
    EnergyComponent,
    HomeComponent,
    HrComponent,
    InfasturactureComponent,
    ManufacturingComponent,
    MiningComponent,
    ReportComponent,
    TelemaskComponent,
    UndertakingsComponent,
    UserComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
