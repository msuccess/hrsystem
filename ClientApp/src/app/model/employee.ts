export class Employee {
    Id?: number;
    FirstName: string;
    LastName: string;
    EmployeeCode: string;
    Department: string;
    Position: string;
    Office: string;
    DateOfBirth: Date;


    constructor(data?) {
        data = data || {};
        this.Id = data.Id || 0;
        this.FirstName = data.FirstName || '';
        this.LastName = data.LastName || '';
        this.DateOfBirth = data.DateOfBirth || '';
        this.Department = data.Department || '';
        this.EmployeeCode = data.EmployeeCode || '';
        this.Office = data.Office || '';
        this.Position = data.Position || '';
    }
}
