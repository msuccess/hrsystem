﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HrSystem.Data;
using HrSystem.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HrSystem.Controllers {

    [Route ("api/[controller]")]
     public class HomeController : Controller {
    private readonly IHostingEnvironment host;
         public HomeController(IHostingEnvironment host,HrDbContext _context)
         {
            this.host = host; 
            
         }
        public IActionResult Upload(int vehicleId, IFormFile file)
         {
          var uploadFolderPath =  Path.Combine(host.ContentRootPath,"upload");

          if(!Directory.Exists(uploadFolderPath))
          Directory.CreateDirectory(uploadFolderPath);

          var fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);

          var filePath = Path.Combine(uploadFolderPath, fileName);

          using( var stream = new FileStream(fileName,FileMode.Create))
          {
              file.CopyTo(stream);
          }
          var photo = new Photo {FileName = fileName };
          return Ok();
         }
    }
}