using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using HrSystem.Data;
using HrSystem.Models;
using HrSystem.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HrSystem.Controllers {
    [Route ("api/[controller]")]
    public class EmployeeController : Controller {
        private readonly HrDbContext _context;
        private readonly IMapper _mapper;
        public EmployeeController (HrDbContext context, IMapper mapper) {
            this._context = context;
            this._mapper = mapper;
        }

        //Get all Employees
        [HttpGet]
        public async Task<IActionResult> GetEmployees () {
            var employees = await _context.Employees.ToListAsync ();
            return Ok (employees);
        }

        //Get Id By Employee
        [HttpGet ("{Id}")]
        public async Task<IActionResult> GetEmployeeByIdAsync (int Id) {
            var employee = await _context.Employees.FindAsync (Id);
            return Ok (employee);
        }

        //Create New Employee
        [HttpPost]
        public async Task<IActionResult> CreateEmployee ([FromBody] EmployeeView employeeView) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }
            var employee = _mapper.Map<EmployeeView, Employee> (employeeView);
            await _context.Employees.AddAsync (employee);
            await _context.SaveChangesAsync ();
            return Ok (employee);
        }

        //Update Employee
        [HttpPut ("{Id}")]
        public async Task<IActionResult> UpdateEmployee (int Id, [FromBody] EmployeeView employeeView) {
            if (!ModelState.IsValid) {
                return BadRequest (ModelState);
            }
            var result = _context.Employees.Find (Id);
            if (result != null) {
                var employee = _mapper.Map<EmployeeView, Employee> (employeeView, result);
                _context.Employees.Update (employee);
                await _context.SaveChangesAsync ();
                return Ok (employee);
            }
            return NotFound ();
        }

        //Delete Employee
        [HttpDelete ("{Id}")]
        public async Task<IActionResult> DeleteEmployee (int Id) {
            var result = await _context.Employees.FindAsync (Id);
            if (result != null) {
                var employee = _context.Employees.Remove (result);
                await _context.SaveChangesAsync ();
                return Ok ();
            }
            return NotFound ();
        }

      
    }
}