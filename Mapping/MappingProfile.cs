using AutoMapper;
using HrSystem.ViewModels;
using HrSystem.Models;
namespace HrSystem.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Employee,EmployeeView>();
        }
    }
}