using System;

namespace HrSystem.ViewModels
{
    public class EmployeeView
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmployeeCode { get; set; }
        public string Position { get; set; }
        public string Office { get; set; }
        public string Department { get; set; }
        public DateTime DateOfBirth { get; set; }
    }
}