using HrSystem.Models;
using Microsoft.EntityFrameworkCore;

namespace HrSystem.Data {
    public class HrDbContext : DbContext {
        public HrDbContext (DbContextOptions<HrDbContext> options) : base (options) {}   

        public DbSet<Photo> Photos { get; set; }
        public DbSet<Employee> Employees { get; set; }
    }
}