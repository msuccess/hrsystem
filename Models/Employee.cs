using System;
using System.ComponentModel.DataAnnotations;

namespace HrSystem.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(20)]  
        public string LastName { get; set; }

        [Required]
        [StringLength(20)]    
        public string EmployeeCode { get; set; }

        [Required]
        [StringLength(50)]
        public string Position { get; set; }

        [Required]
        [StringLength(200)]
        public string Office { get; set; }

        [Required]
        [StringLength(200)]
        public string Department { get; set; }

        [Required]
        public DateTime DateOfBirth { get; set; }
    }
}